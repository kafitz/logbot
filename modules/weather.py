#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Kyle Fitzsimmons, 2015
from datetime import datetime
import requests

def current(db):
    location = 'Montreal,QC'
    parameters = {
        'q': location
    }
    url = 'http://api.openweathermap.org/data/2.5/weather'
    now = datetime.now().replace(microsecond=0)
    response = requests.get(url, params=parameters)
    if response.status_code is 200:
        data = response.json()
        celsius = data['main']['temp'] - 273.15
        fahrenheit = (celsius * 1.8) + 32
        weather_entry = {
            'time': now,
            'temperature_c': celsius,
            'temperature_f': fahrenheit,
            'humidity': data['main']['humidity'],
            'sunrise': data['sys']['sunrise'],
            'sunset': data['sys']['sunset'],
            'cloud_cover': data['clouds']['all'],
        } 
        
        db.insert('weather', weather_entry)
        log_msg = '{now}--Weather: {c:.1f}°C/{f:.1f}°F, RH: {rh}, Cloud cover: {cc}%'.format(
            now=now,
            c=celsius,
            f=fahrenheit,
            rh=data['main']['humidity'],
            cc=data['clouds']['all']
            )
    else:
        log_msg = '{now}--Weather: Update failed ({status})'.format(now=now, status=response.status_code)
    irc_msg = log_msg
    return log_msg, irc_msg

if __name__ == '__main__':
    # relative import since not part of a package
    import sys, os
    sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
    from databaser import Database
    db = Database('../testdb.sqlite')
    log_msg, irc_msg = current(db)
    print(log_msg)

