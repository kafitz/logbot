#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Kyle Fitzsimmons, 2015
import unittest
from datetime import datetime

from modules import arduino
from modules import craigslist
from modules import transitfeeds
from modules import waterlevels
from modules import weather

# global inputs
dbfile = './testdb.sqlite'
now = datetime.now().replace(microsecond=0)

class ArduinoTest(unittest.TestCase):
    def test_connect(self):
        self.assertIsNone(arduino.update_watcher())

class CraigslistTest(unittest.TestCase):
    def test_get_new_postingss(self):
        parameters = {
            'base_url': 'http://montreal.en.craigslist.ca',
            'search_url': '/search/bia',
            'query' : None,
            'minAsk': 20,
            'maxAsk': 10000,
            'sort': 'rel'        
        }
        self.assertNotEqual(craigslist.posts(parameters, now),[])

if __name__ == '__main__':
    unittest.main()